# syntax=docker/dockerfile:1
FROM ubuntu:latest

# updating and installing sudo, gcc, lsb-release, gnupg2, vim, curl, wget, locales.
RUN apt-get update && apt-get install -y sudo lsb-release gnupg2 vim curl wget locales && rm -rf /var/lib/apt/lists/* \
    && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8

# ENVs
## setting english
ENV LANG en_US.utf8
## for apt to be noninteractive
ENV DEBIAN_FRONTEND noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN true

# adding nodejs repo
RUN curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -

# adding postgresql repository
# RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ focal-pgdg main" > /etc/apt/sources.list.d/postgresql.list
# RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

RUN apt-get update \
 && apt-get install -y \
      git \
      gcc \
      g++ \
      postgresql-client \
      autoconf \
      automake \
      bison \
      gawk \
      make \
      imagemagick \
      libbz2-dev \
      libcurl4-openssl-dev \
      libevent-dev \
      libffi-dev \
      libgdbm-dev \
      libglib2.0-dev \
      libgmp-dev \
      libjpeg-dev \
      libmagickcore-dev \
      libmagickwand-dev \
      libmysqlclient-dev \
      libncurses-dev \
      libpq-dev \
      libreadline-dev \
      libsqlite3-dev \
      libssl-dev \
      libxml2-dev \
      libxslt-dev \
      libyaml-dev \
      zlib1g-dev \
      libncurses5-dev \
      sqlite3 \
      patch \
      nodejs \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*



# adding user and password
RUN useradd -ms /bin/bash rubyuser &&  echo rubyuser:pass | chpasswd –crypt-method=SHA512  && adduser rubyuser sudo

# logging by default to rubyuser and his homefolder
USER rubyuser
WORKDIR /home/rubyuser

SHELL ["/bin/bash", "-lc"]
CMD ["/bin/bash", "-l"]

# installing nvm
# Node.js via NVM
# ENV \
#     NVM_VERSION=0.38.0 \
#     NODE_VERSION=16.9.1
# # Don't use -x here - node/nvm print just too much stuff
# RUN set -e; \
#     # Manual nvm installation
#     NVM_DIR="$HOME/.nvm"; \
#     PROFILE="$HOME/.profile"; \
#     git clone --branch "v$NVM_VERSION" --depth 1 https://github.com/creationix/nvm.git "$NVM_DIR"; \
#     echo >> "$PROFILE"; \
#     echo 'export NVM_DIR="$HOME/.nvm"' >> "$PROFILE"; \
#     echo '[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm' >> "$PROFILE"; \
#     echo '[ -s "$NVM_DIR/bash_completion" ] && . "$NVM_DIR/bash_completion" # This loads nvm bash_completion' >> "$PROFILE"; \
#     # Load nvm and install the default nodejs version
#     . $NVM_DIR/nvm.sh; \
#     nvm install $NODE_VERSION;

# # installing yarn
# RUN npm install -g yarn
# RUN npm install -g pnpm

# # installing vue-cli
# RUN pnpm i --global @vue/cli

# installing rvm
## For a progress bar when downloading RVM / Rubies
RUN echo progress-bar >> ~/.curlrc

RUN set -ex && \
  for key in \
     409B6B1796C275462A1703113804BB82D39DC0E3 \
     7D2BAF1CF37B13E2069D6956105BD0E739499BDB \
  ; do \
      gpg --batch --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys "$key" || \
      gpg --batch --keyserver hkp://ipv4.pool.sks-keyservers.net --recv-keys "$key" || \
      gpg --batch --keyserver hkp://pgp.mit.edu:80 --recv-keys "$key" ; \
  done


RUN curl -sSL https://get.rvm.io | bash -s -- --autolibs=read-fail stable \
 && echo 'bundler' >> /home/rubyuser/.rvm/gemsets/global.gems \
 && echo 'rvm_silence_path_mismatch_check_flag=1' >> ~/.rvmrc

# Install Rubies
RUN rvm install 2.7.2 \
 && rvm alias create 2.7 ruby-2.7.2
RUN rvm install 3.0.0 \
 && rvm alias create 3.0 ruby-3.0.0
RUN rvm use --default 3.0.0
RUN rvm docs generate-ri

RUN gem install rails bundler shopify-cli

RUN mkdir -p ~/.config/shopify \
    && printf "[analytics]\nenabled = false\n" > ~/.config/shopify/config

RUN echo "### Load RVM into a shell session *as a function*" >> "$HOME/.bashrc" \
    && echo "[[ -s \"$HOME/.rvm/scripts/rvm\" ]] && source \"$HOME/.rvm/scripts/rvm\"" >> "$HOME/.bashrc"

WORKDIR /home/rubyuser/project

EXPOSE 9292
EXPOSE 3456